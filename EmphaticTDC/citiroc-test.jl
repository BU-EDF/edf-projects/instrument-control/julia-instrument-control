#
# test program for serial interface to CITIROC chip
# 
    
using SerialPorts

# send a command to serial port and return echoed string
function do_cmd(s,c)
    write(s, c)
    sleep( 0.1)
    return( replace( readavailable(s), "\r" => ""))
end


# initialize the port
s = SerialPort( "/dev/ttyACM0", 9600)

println(do_cmd( s, "W 30000000 9\r"))    # set the data to binary 1001
println(do_cmd( s, "W 30000001 80\r"))   # set the clock divider to 128
println(do_cmd( s, "W 30000002 4\r"))    # set the number of bits to 4

sleep( 0.3)

# issue a warm reset
println(do_cmd( s, "o 10 0\r"))

sleep( 0.3)

# loop triggering the serial output at 1Hz
while true
    println( do_cmd( s, "W 30000010 0\r"))
    sleep(1)
end
